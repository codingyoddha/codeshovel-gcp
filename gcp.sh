#!/usr/bin/env bash

ZONE="europe-west3-b"
PROJECT=$GOOGLE_CLOUD_PROJECT
MACHINE_TYPE="e2-standard-$2"
SERVICE_ACCOUNT="1050356003682-compute@developer.gserviceaccount.com"
#p
#SERVICE_ACCOUNT="371669974776-compute@developer.gserviceaccount.com"

gcloud compute instances create $1 --project=$PROJECT --zone=$ZONE --machine-type=$MACHINE_TYPE --network-interface=network-tier=STANDARD,subnet=default --no-restart-on-failure --maintenance-policy=TERMINATE --preemptible --service-account=$SERVICE_ACCOUNT --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --create-disk=auto-delete=yes,boot=yes,device-name=$1,image=projects/debian-cloud/global/images/debian-10-buster-v20210916,mode=rw,size=15,type=projects/$PROJECT/zones/$ZONE/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any


#gsutil cp gs://codeshovel/startup.sh .


sleep 20
gcloud compute scp startup.sh $1:~/ --zone=$ZONE --project=$PROJECT

sleep 10
gcloud compute ssh --project=$PROJECT --zone=$ZONE $1 --command '. startup.sh'

echo "_____________ $1 ________________"



