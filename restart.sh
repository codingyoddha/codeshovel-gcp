#!/usr/bin/env bash

ZONE="europe-west3-c"


gcloud compute instances stop $1 --zone=$ZONE


sleep 90


gcloud compute instances start $1 --zone=$ZONE

sleep 20
gcloud compute scp restartup.sh $1:~/ --zone=$ZONE


sleep 10
gcloud compute ssh --zone=$ZONE $1 --command '. restartup.sh'