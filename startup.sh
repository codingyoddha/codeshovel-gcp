#!/usr/bin/env bash

sudo apt-get -y update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common sudo git htop python3-venv

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"

sudo apt-get -y update

sudo apt-get install -y  docker-ce

sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

mkdir -p /tmp/codeshovel/logs80
mkdir -p /tmp/codeshovel/logs4001
mkdir -p /tmp/codeshovel/cache

mkdir -p /home/prasham_ojha/codeshovel

git clone https://gitlab.com/codingyoddha/codeshovel-gcp.git /home/prasham_ojha/codeshovel/

cd /home/prasham_ojha/codeshovel

export IPADDRESS=$(curl https://ipinfo.io/ip)
export PUBLICHOSTNAME="http://$IPADDRESS"
export GITHUB_TOKEN=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
export CODESHOVEL_LOG_PATH_80=/tmp/codeshovel/logs80
export CODESHOVEL_LOG_PATH_4001=/tmp/codeshovel/logs4001
export CODESHOVEL_CACHE_PATH=/tmp/codeshovel/cache

sudo -E docker-compose build
sudo -E docker-compose up -d


command="sh /home/prasham_ojha/codeshovel/cron.sh"
job="4 * * * * $command"
cat <(fgrep -i -v "$command" <(crontab -l)) <(echo "$job") | crontab -


mkdir -p /home/prasham_ojha/restart

git clone https://gitlab.com/codingyoddha/codeshovel-service-restart.git  /home/prasham_ojha/restart

cd  /home/prasham_ojha/restart

python3 -m venv .venv

source /home/prasham_ojha/restart/.venv/bin/activate

pip install -r requirements.txt

nohup python main.py >> nohup.log 2>&1 &

#jobs -l





